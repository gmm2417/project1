"""
Created on Tue Oct 29 14:07:31 2019

@author: gabri
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 15:33:57 2019

@author: gabri
Gabriel Moryoussef
CID:01377549
"""

import numpy as np
import matplotlib.pyplot as plt
from math import e
import pylab

#--------------------------------

def simulate1(N=100,L=8,s0=0.2,r0=1,A=0.2,Nt=100):
    """Part1: Simulate bacterial colony dynamics
    Input:
    N: number of particles
    L: length of side of square domain
    s0: speed of particles
    r0: particles within distance r0 of particle i influence direction of motion
    of particle i
    A: amplitude of noise
    dt: time step
    Nt: number of time steps

    Output:
    X,Y: position of all N particles at Nt+1 times
    alpha: alignment parameter at Nt+1 times

    Do not modify input or return statement without instructor's permission.

    Add brief description of approach to problem here:
    """
    #pl conditions 
    phi_init = np.random.rand(N)*(2*np.pi)
    r_init = np.sqrt(np.random.rand(N))
    Xinit,Yinit = r_init*np.cos(phi_init),r_init*np.sin(phi_init)
    Xinit+=L/2
    Yinit+=L/2
    X=np.zeros(shape = (Nt, N), dtype = 'float')
    Y=np.zeros(shape = (Nt, N), dtype = 'float')
    X[0,:],Y[0,:]=Xinit,Yinit
    theta=np.zeros(shape = (Nt, N), dtype = 'float')
    theta[0,:] = np.random.rand(N)*(2*np.pi) #initial directions of motion
    
   
    alpha=np.zeros(Nt,dtype='float')
    alpha[0]=(1/N)*np.abs(np.dot(e**(theta[0,:]*(1j)),np.ones(N)))
   
    #initialize matrix of pairwise distance 
    D=np.zeros(shape = (N,N), dtype = 'float')
    
    #loop through all the time 
    for t in range(1,Nt):
        #calculate the matrix of pairwise distance between bacteria at time j
        D=np.sqrt(((np.outer(X[t-1,:],np.ones(N,dtype='float'))-np.repeat([X[t-1,:]],repeats=N,axis=0))**2+(np.outer(Y[t-1,:],np.ones(N,dtype='float'))-np.repeat([Y[t-1,:]],repeats=N,axis=0))**2))
        
        #Transform D such that D_ij=1 if D_ij<r0 and O elsewhere
        D=np.where(D<r0,1,0)
     
        #add values of theta for all bacteria at time t
        theta[t,:]=np.angle(np.dot(D,e**(theta[t-1,:]*(1j)))+ A*np.multiply(np.sum(D,axis=1),e**((1j)*np.random.rand(N)*(2*np.pi))))
    
        X[t,:]=np.mod(X[t-1,:]+s0*np.cos(theta[t,:]),L)
        Y[t,:]=np.mod(Y[t-1,:]+s0*np.sin(theta[t,:]),L)
        alpha[t]=(1/N)*(np.abs(np.dot(e**((1j)*theta[t,:]),np.ones(N))))
       
       
   #plt.plot(X[0,:],Y[0,:],".")
   
   # plt.plot(X[Nt-1,:],Y[Nt-1,:],".")
    #plt.show()
    return (X,Y,alpha)
   
    
def analyze(A) :
    
    (X,Y,alpha)=simulate1(100,8,0.2,1,A,100)
    
    #print the last 20 times of alpha to analyze after a possible transient
    #print(alpha[90:])
    
    #print the mean and variance of the last 20 values of alpha to analyze 
    #the stationary behaviour of alpha
    mean=np.mean(alpha[90:])
    var=np.var(alpha[90:])
    
    return mean,var,alpha[90:]



if __name__ == '__main__':
    #The code here should call analyze and
    #generate the figures that you are submitting with your
    #discussion.

   

    W=np.zeros(61)  
     
    for i in range(60):
          
          mean1,var1,alpha1=analyze(0.2+i/100)
          mean2, var2,alpha2 = analyze(0.2+(i+1)/100)
          
          delta=np.abs(mean2-mean1)
          W[i]=delta
        
          A_estimate=np.where(W==np.max(W))[0][0]
          A_estimate=0.2+A_estimate/100
    
          
    print(A_estimate)
    
    #generate a plot of the values of alpha for different A
    plt.figure(1)
    x=np.linspace(1,100,100)  
    for A in np.linspace(0.2,0.8,7):
        
            (X,Y,alpha)=simulate1(100,8,0.2,1,A,100)
            pylab.plot(x,alpha)
            pylab.title('Plot of alpha for different values of A')
            
    plt.figure(2)
    x=np.linspace(1,100,100)        
    
    #investigate the dependance of alpha on 1-A/A
    for j in np.linspace(0.025/A_estimate,0.8-0.2/A_estimate,10):
            (X,Y,alpha)=simulate1(100,8,0.2,1,j,100)
            pylab.plot(x,alpha)
            pylab.title('Dependance of alpha on (1-A/A*)')
            
    #plot of alpha versus A
    plt.figure(3)
    for A in np.linspace(0.2,0.8,200):
        
            (X,Y,alpha)=simulate1(100,8,0.2,1,A,200)
            plt.plot(A,alpha[199],".")
            plt.show()
            
            
    
    
    
